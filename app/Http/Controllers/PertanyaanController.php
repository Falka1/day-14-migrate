<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('post.index',compact('pertanyaan'));
    }

    public function create()
    {
        return view('post.create');
    }
    public function store(Request $request)
    {
        //dd($request->all());
        $request -> validate([
            "judul" => 'required|unique:pertanyaan',
            "isi" => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request['judul'],
            "isi" => $request['isi']
            ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');
    }
    public function show($id)
    {
        $post = DB::table('pertanyaan')->where('pertanyaan_id',$id)->first();
        // dd($post);
        return view('post.show', compact('post'));
        
    }
    public function edit($id)
    {
        $post = DB::table('pertanyaan')->where('pertanyaan_id',$id)->first();
        // dd($post);
        return view('post.edit', compact('post'));
        
    }
    public function update($id, Request $request)
    {
        $request -> validate([
            "judul" => 'required|unique:pertanyaan',
            "isi" => 'required'
        ]);
        $query = DB::table('pertanyaan')
                        ->where('pertanyaan_id', $id)
                        ->update([
                            "judul" => $request['judul'],
                            "isi" => $request['isi']
                        ]);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Diupdate!');
    }
    public function destroy($id)
    {
        $query = DB::table('pertanyaan')->where('pertanyaan_id',$id)->delete();
        // dd($post);
        return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Dihapus!');
        
    }
}

