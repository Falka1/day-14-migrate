@extends('master')

@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Post ke-{{$post->pertanyaan_id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action='/pertanyaan/{{$post->pertanyaan_id}}' method='POST'>
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name='judul' value="{{old('judul',$post->judul)}} " placeholder="Masukkan Judul">
                    @error('judul')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                        <label>isi</label>
                        <textarea class="form-control" id="isi" name="isi" rows="3"  placeholder="Enter ...">{{old('isi',$post->isi)}}</textarea>
                        @error('isi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <a href='/pertanyaan' class="btn btn-warning">Kembali</a>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
@endsection
