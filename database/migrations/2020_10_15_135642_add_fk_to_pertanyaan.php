<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->unsignedBigInteger('profile_id')->nullable()->unsigned();
            $table->foreign('profile_id')->references('profile_id')->on('profil');
            $table->unsignedBigInteger('jawaban_id')->nullable()->unsigned();
            $table->foreign('jawaban_id')->references('jawaban_id')->on('jawaban');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->dropForeign(['profile_id']);
            $table->dropForeign(['jawaban_id']);
            $table->dropColumn(['profile_id']);
            $table->dropColumn(['jawaban_id']);
        });
    }
}
