@extends('master')

@section('content')
<div class="card">
        <div class="card-header">
          <h3 class="card-title">{{$post->judul}}</h3>

          
        </div>
        <div class="card-body">
        {{$post->isi}}
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        <a class="btn btn-info btn-sm" href="/pertanyaan">Balik</a>
        </div>
        <!-- /.card-footer-->
      </div>


@endsection
